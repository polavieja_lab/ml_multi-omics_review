# ML_Multi-Omics_Review

This repo includes the data and code to produce the figures for our review on ML and multi-omics.

Code was developed using python 3.8

Dependencies:

    openpyxl
    pandas
    matplotlib
    numpy
    sys
    seaborn
    scipy
