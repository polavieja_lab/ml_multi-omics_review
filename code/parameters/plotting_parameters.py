# Plotting parameters for the multi-omics review


# RGB codes from Seaborn's colorblind library
gold = (211/255, 147/255, 52/255)
blue = (49/255, 113/255, 173/255)
purple = (192/255, 124/255, 184/255)
green = (70/255, 156/255, 118/255)
yellow = (234/255, 224/255, 89/255)
orange = (198/255, 101/255, 38/255)
lightblue = (111/255, 178/255, 228/255)
greybrown = (193/255, 147/255, 104/255)
pink = (240/255, 178/255, 225/255)
grey = (148/255, 148/255, 148/255)

def standard_bar_colour():
    return (91/255, 101/255, 173/255) # Indigo

def axis_tick_size():
    return 12

def omics_colours():

    colours = {'Genomics': lightblue,
                'Metabolomics': yellow,
                'Metagenomics': greybrown,
                'Other': purple,
                'Proteomics' : grey,
                'Transcriptomics' : green,
                'Epigenomics' : orange}

    return colours

def dataset_colours():

    colours = {'TCGA': blue, # blue
                'Combination': purple, # purple
                'Other': orange, # red
                'Single': green, # green
                'Own' : gold} # yellow

    return colours
